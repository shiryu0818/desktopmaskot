﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopMaskot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        // 起動前の読み込み時イベントハンドラ
        private void Form1_Load_1(object sender, EventArgs e)
        {
            string Path = FilePath();
            // マスコット画像のファイル読み込み
            Image Maskot = Image.FromFile(Path);
            // 描画の設定
            show(Maskot);
        }

        // 特定の画像ファイルのパスを取得
        private string FilePath()
        {
            string filePath = null;
            
            // 現在実行中のコードのアセンブリを特定してAssemblyクラスのオブジェクトに格納
            System.Reflection.Assembly MyAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            // アセンブリのパスを取得
            string AbsolutelyPath = MyAssembly.Location;
            // アセンブリパスと画像のパスをリンクさせる
            Uri u1 = new Uri(AbsolutelyPath);
            Uri u2 = new Uri(u1,"..\\DesktopMaskot\\マスコット.png");
            
            // リンクさせたパスをstring型の変数に格納
            filePath = u2.ToString();

            // リンクさせたパスの余分な文字列を除いた文字列を格納しなおす
            filePath = filePath.Substring(8);   // 8文字目から格納

            // テスト出力(指しているパス)
            Console.WriteLine(filePath);


            return filePath;
        }

        // 画像の設定
        private void show(Image Maskot)
        {
            //フォームの境界線をなくす
            this.FormBorderStyle = FormBorderStyle.None;
            // フォームのサイズ変更
            SizeChange(Maskot);
            // 画像を等倍で描画させる
            this.BackgroundImageLayout = ImageLayout.Stretch;
            // 背景画像の登録
            this.BackgroundImage = Maskot;
            //部分を透明化する
            this.TransparencyKey = Color.White;
        }

        // ウィンドウの大きさを画像の大きさに変更
        private void SizeChange(Image Maskot)
        {
            // 元画像の縦横サイズを調べる
            int width = Maskot.Width;
            int height = Maskot.Height;

            // 画像の大きさを比率を保持したまま一定のサイズまで縮小
            float Ix = width, Iy = height;
		    float Md = 1.00f, d = 0.01f;
		    if(Ix > Iy){
			    // Ixの比率を計算した結果が500を下回るまで割合計算
			    for(;Ix * Md > 400;Md -= d);
			    Ix *= Md; Iy *= Md;
		    }
		    else if(Ix < Iy){
			    // Iyの比率を計算した結果が500を下回るまで割合計算
			    for(;(double)Iy * Md > 400;Md -= d);
			    Iy *= Md; Ix *= Md;
		    }
            // ウィンドウサイズの変更
            this.Size = new Size((int)Ix, (int)Iy);
        }

        // 座標保管用変数
        int Mx; int My;

        // マウスが押された時の発生イベントハンドラ
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            // 押されたのがマウスの右ボタンだったら通る
            if (e.Button == MouseButtons.Right)
            {
                // メニューの表示とコントロールをtrueに
                menuStrip1.Visible = true; menuStrip1.Enabled = true;
                // メニューの表示座標を更新
                menuStrip1.Location = new Point(e.X, e.Y);  // コントロール内での座標限定なのでMouseEventで

                // テスト用出力
                Console.WriteLine("menuStrip1.Visible " + menuStrip1.Visible);
                // テスト用出力
                Console.WriteLine("menuStrip1.Enabled " + menuStrip1.Enabled);
            }
            // 押されたマウスのボタンが左だったら以下の処理をしない
            else if (e.Button == MouseButtons.Left)
            {
                // マウスの座標を保管
                this.Mx = Control.MousePosition.X; this.My = Control.MousePosition.Y;   // コントロール外にも対象なのでMousePositionで
                // テスト用出力
                Console.WriteLine("保管時のMx,My : " + this.Mx + "," + this.My);
            }
        }

        // マウスのボタンが離れた時のイベントハンドラ
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            // 離されたマウスのボタンが右以外だったら以下の処理をしない
            if (e.Button != MouseButtons.Left) return;

            // ボタンを押したときの座標と離した現在の場所の座標の差分を求める
            int x = Control.MousePosition.X - this.Mx; int y = Control.MousePosition.Y - this.My;

            // 現在表示されているデスクトップ上の座標から
            this.DesktopLocation = new Point( this.DesktopLocation.X + x , this.DesktopLocation.Y + y);
            // テスト用出力
            Console.WriteLine("DesktopLocation.X" + (this.DesktopLocation.X + x) + ", DesktopLocation.Y" + (this.DesktopLocation.Y + y));
            // テスト用出力
            Console.WriteLine("比較時のMx,My : " + this.Mx + "," + this.My);
        }

        #region        MenuMethod

        // メニュー＞ツール＞常に手前表示のチェック
        private void TopMostToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 現在とは逆の状態を格納
            this.TopMost = !this.TopMost;
        }

        // メニュー＞閉じる
        private void MenuCloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // メニューの表示とコントロールをfalseに
            this.menuStrip1.Visible = false; this.menuStrip1.Enabled = false;
        }

        // メニュー＞終了
        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 終了する
            this.Close();
        }

        #endregion

    }
}
